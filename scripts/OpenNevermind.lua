if require('openmw.core').API_REVISION < 22 then
    error('This mod requires a newer version of OpenMW, please update.')
end

local core = require('openmw.core')
local camera = require('openmw.camera')
local input = require('openmw.input')
local self = require('openmw.self')
local util = require('openmw.util')
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')
local types = require('openmw.types')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')

local Actor = types.Actor
local Item = types.Item
local Weapon = types.Weapon

local health = Actor.stats.dynamic.health

local settings = storage.playerSection('SettingsOpenNevermind')

I.Settings.registerPage({
  key = 'OpenNevermind',
  l10n = 'OpenNevermind',
  name = 'OpenNevermind',
  description = 'OpenNevermindDescription',
})

local function boolSetting(key, default)
    return {
        key = key,
        renderer = 'checkbox',
        name = key,
        description = key..'Description',
        default = default,
    }
end
local function floatSetting(key, default)
    return {
        key = key,
        renderer = 'number',
        name = key,
        description = key..'Description',
        default = default,
    }
end

I.Settings.registerGroup({
    key = 'SettingsOpenNevermind',
    page = 'OpenNevermind',
    l10n = 'OpenNevermind',
    name = 'Settings',
    permanentStorage = true,
    settings = {
        boolSetting('usePathFinding', true),
        floatSetting('aimingTime', 1.6),
    },
})

if I.Camera then
    I.Camera.disableModeControl()
    I.Camera.disableStandingPreview()
    I.Camera.disableThirdPersonOffsetControl()
    I.Camera.disableZoom()
end
camera.setFocalPreferredOffset(util.vector2(0, 0))

local MODE = camera.MODE

local cursor_normal = ui.texture{path = 'textures/tx_cursor.dds'}
local cursor_clicked = ui.texture{path = 'textures/cursor_drop_ground.dds'}
local cursor_item = ui.texture{path = 'textures/OpenNevermind/backpack.png'}
local cursor_sword = ui.texture{path = 'textures/OpenNevermind/sword.png'}
local cursor_bow = ui.texture{path = 'textures/OpenNevermind/bow.png'}
local cursor_talk = ui.texture{path = 'textures/OpenNevermind/envelope.png'}
local cursor_map = ui.texture{path = 'textures/OpenNevermind/map.png'}
local cursor_spell = ui.texture{path = 'textures/OpenNevermind/scroll.png'}

local element = ui.create {
  layer = 'Windows',
  type = ui.TYPE.Image,
  props = {
    size = util.vector2(40, 40),
    resource = cursor_normal,
  },
}

local iconSize = 80

local function createIcon(pos, texture)
    return ui.create {
      layer = 'HUD',
      type = ui.TYPE.Image,
      props = {
        size = util.vector2(iconSize, iconSize),
        anchor = util.vector2(0.5, 0.5),
        resource = texture,
        relativePosition = pos,
      },
    }
end

local toggleWeaponPos = util.vector2(0.45, 0.1)
local toggleMagicPos = util.vector2(0.55, 0.1)

local toggleWeapon = createIcon(toggleWeaponPos, cursor_sword)
local toggleMagic = createIcon(toggleMagicPos, cursor_spell)

local camPitch = math.rad(80)
local camYaw = 0
local camDist = 800

local cursorPos = util.vector2(0, 0)
local mouseButtonWasPressed = false

local destination = nil
local target = nil
local attackTarget = false
local magicAttack = false
local marksmanAttack = nil
local path = nil
local pathIndex = 1
local interactionDistance = 100

local function buildPath()
    if not nearby.findPath or not settings:get('usePathFinding') then
        path = nil
        return
    end
    if target then destination = target.position end
    nearby.findPath(self.position, destination)
    local status
    status, path = nearby.findPath(self.position, destination,
        { includeFlags = nearby.NAVIGATOR_FLAGS.Walk + nearby.NAVIGATOR_FLAGS.Swim + nearby.NAVIGATOR_FLAGS.UsePathgrid })
    if status == nearby.FIND_PATH_STATUS.Success then
        pathIndex = 1
        local pathLength = 0
        for i = 1, #path - 1 do
            pathLength = pathLength + (path[i] - path[i + 1]):length()
        end
        if pathLength > (destination - self.position):length() * 3 then
            path = nil
        end
    else
        path = nil
    end
end

local function checkTarget(ray)
    if ray.hitObject and Actor.objectIsInstance(ray.hitObject) then return true end
    local delta = ray.hitPos - self.position
    return delta.z < 160 or delta.z < 0.5 * delta:length()
end

local pointedDest = nil
local pointedTarget = nil
local pointedCursor = cursor_normal

local function targetClassification(newDest, newTarget)
    pointedCursor = cursor_normal
    if newTarget and newTarget.type ~= types.Static and newTarget.recordId ~= 'chargen boat'
        and (not types.ESM4Static or newTarget.type ~= types.ESM4Static)
    then
        pointedTarget = newTarget
        pointedDest = newTarget.position
        if Actor.objectIsInstance(newTarget) then
            if health(newTarget).current <= 0 then
                pointedCursor = cursor_item
            elseif Actor.stance(self) == Actor.STANCE.Weapon then
                pointedCursor = cursor_sword
            elseif Actor.stance(self) == Actor.STANCE.Spell then
                pointedCursor = cursor_spell
            elseif types.NPC.objectIsInstance(newTarget) then
                pointedCursor = cursor_talk
            end
        elseif types.Door.objectIsInstance(newTarget) or (types.ESM4Door and types.ESM4Door.objectIsInstance(newTarget)) then
            pointedCursor = cursor_map
        elseif Item.objectIsInstance(newTarget) or types.Container.objectIsInstance(newTarget) then
            pointedCursor = cursor_item
        end
    else
        pointedTarget = nil
        pointedDest = newDest
    end
    if pointedTarget == self.object and pointedCursor ~= cursor_spell then
        pointedCursor = cursor_normal
        pointedTarget = nil
        pointedDest = newDest
    elseif pointedCursor == cursor_sword then
        local item = Actor.equipment(self, Actor.EQUIPMENT_SLOT.CarriedRight)
        local weaponRecord = item and item.type == Weapon and Weapon.record(item)
        if weaponRecord then
            local t = weaponRecord.type
            if t == Weapon.TYPE.MarksmanBow or t == Weapon.TYPE.MarksmanCrossbow or t == Weapon.TYPE.MarksmanThrown then
                pointedCursor = cursor_bow
            end
        end
    end
end

local function findObjectUnderMouse()
    local delta = camera.viewportToWorldVector(cursorPos:ediv(ui.screenSize()))
    local basePos = camera.getPosition() + delta * 50
    local endPos = basePos + delta * 10000
    local res = nearby.castRay(basePos, endPos)
    if not res.hitObject then
        res = nearby.castRenderingRay(basePos, endPos)
    end
    while res.hitPos and not checkTarget(res) do
        local res1 = nearby.castRenderingRay(res.hitPos + delta * 20, endPos)
        local res2 = nearby.castRay(res.hitPos, endPos, {ignore=res.hitObject})
        if not res1.hitPos or (res2.hitPos and (res2.hitPos-basePos):length2() < (res1.hitPos-basePos):length2()) then
            res = res2
        else
            res = res1
        end
    end
    targetClassification(res.hitPos, res.hitObject)
end

local function onFrame(dt)
    if core.isWorldPaused() or (I.UI and I.UI.getMode()) or camera.getMode() == MODE.FirstPerson then
        element.layout.props.visible = false
        element:update()
        return
    end

    -- Mouse
    local mouseButtonPressed = input.isMouseButtonPressed(1) or input.isControllerButtonPressed(input.CONTROLLER_BUTTON.A)

    local screenSize = ui.screenSize()
    cursorPos = cursorPos + util.vector2(input.getMouseMoveX(), input.getMouseMoveY())
    local controllerCoef = math.min(screenSize.x, screenSize.y) * (dt * 1.5)
    cursorPos = cursorPos + util.vector2(
        input.getAxisValue(input.CONTROLLER_AXIS.MoveLeftRight),
        input.getAxisValue(input.CONTROLLER_AXIS.MoveForwardBackward)) * controllerCoef
    cursorPos = util.vector2(util.clamp(cursorPos.x, 0, screenSize.x), util.clamp(cursorPos.y, 0, screenSize.y))
    element.layout.props.relativePosition = cursorPos:ediv(ui.screenSize())
    findObjectUnderMouse()
    local toggleWeaponHover = (cursorPos - toggleWeaponPos:emul(ui.screenSize())):length() < iconSize / 2
    local toggleMagicHover = (cursorPos - toggleMagicPos:emul(ui.screenSize())):length() < iconSize / 2
    if toggleWeaponHover or toggleMagicHover then
        element.layout.props.resource = cursor_normal
        element.layout.props.anchor = util.vector2(0.2, 0.0)
    elseif mouseButtonPressed then
        element.layout.props.resource = cursor_clicked
        element.layout.props.anchor = util.vector2(0.5, 0.5)
    else
        element.layout.props.resource = pointedCursor
        if pointedCursor == cursor_normal then
            element.layout.props.anchor = util.vector2(0.2, 0.0)
        else
            element.layout.props.anchor = util.vector2(0.5, 0.5)
        end
    end
    element.layout.props.visible = true
    element:update()
    if Actor.stance(self) == Actor.STANCE.Weapon then
        toggleWeapon.layout.props.alpha = 1
        toggleMagic.layout.props.alpha = 0.7
    elseif Actor.stance(self) == Actor.STANCE.Spell then
        toggleWeapon.layout.props.alpha = 0.7
        toggleMagic.layout.props.alpha = 1
    else
        toggleWeapon.layout.props.alpha = 0.7
        toggleMagic.layout.props.alpha = 0.7
    end
    toggleWeapon:update()
    toggleMagic:update()

    -- Mouse click
    if mouseButtonWasPressed and not mouseButtonPressed then
        if toggleWeaponHover then
            if Actor.stance(self) == Actor.STANCE.Weapon then
                Actor.setStance(self, Actor.STANCE.Nothing)
            else
                Actor.setStance(self, Actor.STANCE.Weapon)
            end
        elseif toggleMagicHover then
            if Actor.stance(self) == Actor.STANCE.Spell then
                Actor.setStance(self, Actor.STANCE.Nothing)
            else
                Actor.setStance(self, Actor.STANCE.Spell)
            end
        else
            target = pointedTarget
            destination = pointedDest
            attackTarget = pointedCursor == cursor_sword
            if pointedCursor == cursor_bow and not marksmanAttack then
                marksmanAttack = settings:get('aimingTime')
            elseif pointedCursor == cursor_spell then
                magicAttack = true
            end
            buildPath()
            if target and target.getBoundingBox then
                interactionDistance = self:getBoundingBox().halfSize.x + target:getBoundingBox().halfSize.x + 40
            end
        end
    end
    mouseButtonWasPressed = mouseButtonPressed

    -- Camera
    if input.isKeyPressed(input.KEY.A) then camYaw = camYaw - dt end
    if input.isKeyPressed(input.KEY.D) then camYaw = camYaw + dt end
    if input.isKeyPressed(input.KEY.W) then camPitch = camPitch + dt end
    if input.isKeyPressed(input.KEY.S) then camPitch = camPitch - dt end
    camYaw = camYaw + input.getAxisValue(input.CONTROLLER_AXIS.LookLeftRight) * dt
    camPitch = camPitch + input.getAxisValue(input.CONTROLLER_AXIS.LookUpDown) * dt
    camPitch = util.clamp(camPitch, 0, math.rad(90))
    if camera.getMode() ~= MODE.Static and camera.getMode() ~= MODE.Preview then
        camera.setMode(MODE.Preview)
    end
    camera.showCrosshair(false)
    camera.setPitch(camPitch)
    camera.setYaw(camYaw)
    camera.setRoll(0)
    camera.setPreferredThirdPersonDistance(camDist)
    if camera.getMode() == MODE.Static then
        local offset = util.transform.rotateZ(camYaw) * util.transform.rotateX(camPitch) * util.vector3(0, -camDist, 0)
        camera.setStaticPosition(camera.getTrackedPosition() + offset)
    end

    -- Movement and attack
    self.controls.sideMovement = 0
    self.controls.movement = 0
    self.controls.yawChange = 0
    self.controls.pitchChange = 0
    self.controls.use = 0
    local yawChange = 0
    local dist = 0
    local nextPathPoint = nil
    if target then
        destination = target.position
    end
    local fullDist = 0
    if destination then
        local fullRelVec = destination - self.position
        fullDist = util.vector2(fullRelVec.x, fullRelVec.y):length()
    end
    if target and path and (path[#path] - destination):length() > fullDist * 0.2 then
        buildPath()
    end
    if path then
        while path[pathIndex + 1] and (path[pathIndex] - self.position):length() < 50 do
            pathIndex = pathIndex + 1
        end
        nextPathPoint = path[pathIndex]
    end
    nextPathPoint = nextPathPoint or destination
    if destination and target ~= self.object then
        local pitch, yaw
        if core.API_REVISION < 40 then
            pitch, yaw = self.rotation.x, self.rotation.z
        else
            pitch, yaw = self.rotation:getAnglesXZ();
        end
        local relDest = nextPathPoint - self.position - util.vector3(0, 0, 60)
        local destVec = util.vector2(relDest.x, relDest.y):rotate(yaw)
        yawChange = math.atan2(destVec.x, destVec.y)
        local maxDelta = math.max(math.abs(yawChange), 1) * 2 * dt
        dist = destVec:length()
        self.controls.yawChange = util.clamp(yawChange, -maxDelta, maxDelta)
        self.controls.pitchChange = math.asin(-relDest.z / relDest:length()) - pitch
    end
    local yawAbs = math.abs(yawChange)
    if marksmanAttack then
        if marksmanAttack > 0 or yawAbs > 0.2 then
            self.controls.use = 1
            marksmanAttack = marksmanAttack - dt
        else
            marksmanAttack = nil
            target = nil
            destination = nil
            path = nil
        end
    elseif magicAttack then
        if yawAbs < 0.1 then
            self.controls.use = 1
            magicAttack = false
            target = nil
            destination = nil
            path = nil
        end
    else
        self.controls.movement = math.min(1, math.min(0.4 / (yawAbs+0.1), fullDist / 80))
        self.controls.run = true
        if yawAbs > 0.3 and fullDist < 100 then
            self.controls.movement = 0
        end
        if attackTarget then
            self.controls.use = 1
        end
        if fullDist < 20 or (target and fullDist < interactionDistance and yawAbs < 0.3) then
            if target and not attackTarget then
                target:activateBy(self)
            end
            destination = nil
            path = nil
            target = nil
            attackTarget = false
        end
    end
end

return {
    engineHandlers = {
        onFrame = onFrame,
        onInputAction = function(action)
            if core.isWorldPaused() or (I.UI and I.UI.getMode()) then return end
            if action == input.ACTION.ZoomIn then
                camDist = math.max(50, camDist * 0.95)
            elseif action == input.ACTION.ZoomOut then
                camDist = math.min(5000, camDist / 0.95)
            elseif action == input.ACTION.TogglePOV then
                if camera.getMode() == MODE.FirstPerson then
                    if input.getControlSwitch(input.CONTROL_SWITCH.ViewMode) then
                        camera.setMode(MODE.Static)
                        ui.showMessage("Top-down view")
                    else
                        ui.showMessage("View mode change is not allowed at the moment")
                    end
                elseif camera.getMode() == MODE.Static then
                    camera.setMode(MODE.Preview)
                    ui.showMessage("Top-down view with camera collision")
                else
                    camera.setMode(MODE.FirstPerson)
                end
            end
        end,
        onSave = function()
            return {camPitch = camPitch, camYaw = camYaw, camDist = camDist, cursorPos = cursorPos, mode = camera.getMode()}
        end,
        onLoad = function(data)
            if data and data.camPitch then camPitch = data.camPitch end
            if data and data.camYaw then camYaw = data.camYaw end
            if data and data.camDist then camDist = data.camDist end
            if data and data.cursorPos then cursorPos = data.cursorPos end
            if data and data.mode then camera.setMode(data.mode) end
        end,
    }
}

