# OpenNevermind

Turns OpenMW into an engine for isometric RPGs. Was developed as a [1st April joke](https://openmw.org/2022/openmw-roadmap-update/), but surprisingly it turned out to be actually playable and some people liked it. So now OpenNevermind is available as a mod.

Not compatible with "[Advanced camera](https://gitlab.com/ptmikheev/openmw-lua-examples#advanced-camera)".

### Controls

- Left mouse click -- move to clicked ground or interact with clicked object
- Mouse wheel -- scroll to change camera zoom
- `[W]`, `[S]` -- control camera pitch
- `[A]`, `[D]` -- control camera yaw
- `[TAB]` -- switch camera modes *Classic first person* / *Top down view* / *Top down view with camera collisions* (key binding is equal to `<Toggle POV>` in the settings menu)

The game starts with first person view (otherwise it is not convenient to do the intro quest), can be switched (`[TAB]`) to top down view when the character generation is finished.

### Installation

Download the mod and add the following lines to `openmw.cfg`:
```
# Specify here the path to the files
data="path/to/openmw-lua-examples/OpenNevermind"

content=OpenNevermind.omwscripts
```
